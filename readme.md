This is Windows Hosted(x86_64-w64-mingw32) GCC version 11.0.1 20210409 cross compiler of x86_64-elf and i686-elf toolchains. They build with newlib.

x86_64-elf is red-zone multilibs enabled.

Remove emergency heap in libsupc++. operator new will fail-fast instead of throw std::bad_alloc and global new is now noexcept. Remove libstdc++ verbose terminate handler. std::terminate now calls __builtin_trap(). set_new_terminator (..) will become no-op.

You can still throw exceptions, and the EH runtime bloat now greatly reduces. That allows us to static linking libstdc++ freely.

Although iostream is still usable, I recommend not to use it since iostream bloats binary for 1 MB.
This GCC also contains my fast_io library (https://github.com/expnkx/fast_io) to replace iostream without C++ runtime bloat.

GCC 11 supports stdlib=libc++, but you need to be careful since the version of libc++ in this package is not the latest.